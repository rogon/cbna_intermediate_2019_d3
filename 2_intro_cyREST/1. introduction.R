# Original script by Keiichiro Ono
#
#
#
# load the necessary libraries
library(RJSONIO)
library(igraph)
library(httr)

# Setup the connection with Cytoscape 
# cyREST is found on port 1234 of the localhost
# you can modify these settings under advanced preferences in Cytoscape

port.number = 1234
base.url = paste("http://localhost:", toString(port.number), "/v1", sep="")
print(base.url)

# check the connection
version.url = paste(base.url, "version", sep="/")
cytoscape.version = GET(version.url)
cy.version = fromJSON(rawToChar(cytoscape.version$content))
print(cy.version)

# Create a demo network in iGraph using the Barabasi-Albert model
graphBA <- barabasi.game(150)

# Calculate network centralities
graphBA$name = "Scale-Free BA Model"
graphBA$density = graph.density(graphBA)

V(graphBA)$degree <- degree(graphBA)
V(graphBA)$closeness <- closeness(graphBA)
V(graphBA)$betweenness <- betweenness(graphBA)
V(graphBA)$page_rank <- page.rank(graphBA)$vector
V(graphBA)$community <- label.propagation.community(graphBA)$membership
E(graphBA)$betweenness <- edge.betweenness(graphBA)


# Access visual styles in Cytoscape
# ___default___ style is composed as JSON

default.style.url = paste(base.url, "styles/default", sep="/")
GET(url=default.style.url)

# Access this URL in your web browser
# http://localhots:1234/v1/styles/default

# generate a new style
style.name = "R2 Style"

# Defaults
def.node.color <- list(
  visualProperty = "NODE_FILL_COLOR",
  value = "#00aabb"
)

def.node.border.width <- list(
  visualProperty = "NODE_BORDER_WIDTH",
  value = 0
)

def.node.size <- list(
  visualProperty = "NODE_SIZE",
  value = 25
)

def.edge.target.arrow <- list(
  visualProperty="EDGE_TARGET_ARROW_SHAPE",
  value="ARROW"
)

def.edge.width <- list(
  visualProperty="EDGE_WIDTH",
  value=3
)

defaults <- list(
  def.node.color,
  def.node.border.width,
  def.node.size,
  def.edge.target.arrow,
  def.edge.width)

# Visual Mappings
min.betweenness = min(V(graphBA)$betweenness)
max.betweenness = max(V(graphBA)$betweenness)

mappings = list()

point1 = list(
  value=min.betweenness,
  lesser= "20.0",
  equal="20.0",
  greater="20.0"
)

point2 = list(
  value=max.betweenness,
  lesser="100.0",
  equal="100.0",
  greater="100.0"
)

node.size.continuous.points = list(point1, point2)

node.size = list(
  mappingType="continuous",
  mappingColumn="betweenness",
  mappingColumnType="Double",
  visualProperty="NODE_SIZE",
  points = node.size.continuous.points
)

node.label = list(
  mappingType="passthrough",
  mappingColumn="name",
  mappingColumnType="String",
  visualProperty="NODE_LABEL"
)

mappings = list(node.size, node.label)

style <- list(title=style.name, defaults = defaults, mappings = mappings)
style.JSON <- toJSON(style)

style.url = paste(base.url, "styles", sep="/")
POST(url=style.url, body=style.JSON, encode = "json")


### Sending network to Cytoscape
# Cytoscape developers have provided a JSON conversion file (converts a JSON object to 
# Cytoscape format) - open the utility in a text editor to have a look

# Load utility functions
source("/Volumes/Work/01. Teaching/CBNA Courses/Level 2-3 Graph Theory, Advanced scripting and automation/2019/Day 3 - cyREST/2_intro_cyREST/utility/cytoscape_util.R")

# Convert
cygraph <- toCytoscape(graphBA)

# Send it
network.url = paste(base.url, "networks", sep="/")
res <- POST(url=network.url, body=cygraph, encode="json")

# Extract SUID of the new network
network.suid = unname(fromJSON(rawToChar(res$content)))

### Apply layout algorithm and Visual Style
#Finally, let's apply new Visual Style and layout algorithm:

```{r}
apply.layout.url = paste(
  base.url,
  "apply/layouts/force-directed",
  toString(network.suid),
  sep="/"
)

apply.style.url = paste(
  base.url,
  "apply/styles",
  style.name,
  toString(network.suid),
  sep="/"
)

res <- GET(apply.layout.url)
res <- GET(apply.style.url)


### Getting network view as image

network.image.url = paste(
  base.url,
  "networks",
  toString(network.suid),
  "views/first.png",
  sep="/"
)
print(network.image.url)
