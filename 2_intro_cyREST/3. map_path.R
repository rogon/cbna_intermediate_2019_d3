# original script by Kazuhiro Takemoto
# Updated by Keiichiro Ono for cyREST
# Modified by M.Z. Rogon for the Intermediate Network Analysis course
#
# Visualize a path between two nodes in a network

![](http://cytoscape.org/images/logo/cy3logoOrange.svg)  ![](Rlogo.png)

# Basic setup
library(igraph)
library(RJSONIO)
library(httr)
port.number = 1234
base.url = paste("http://localhost:", toString(port.number), "/v1", sep="")
source('toCytoscape.R')


# Reload the Data
network.df <- read.table("data/eco_EM+TCA.txt")
network <- graph.data.frame(network.df,directed=T)
g.tca <- simplify(network, remove.multiple=T, remove.loops=T)
g.tca$name = "Ec TCA Cycle"
#Find all shortest paths from _D-Glucose_ to _2-Oxoglutarate_
paths <- get.all.shortest.paths(g.tca,"D-Glucose","2-Oxoglutarate",mode="out")
# Nodes in the first path
print(V(g.tca)[paths$res[[1]]])
# Add new attribute "path" and set value 1 for this path
V(g.tca)[paths$res[[1]]]$path <- 1
# Edges in the path
print(E(g.tca,path=paths$res[[1]]))
# Add path attribute
E(g.tca, path=paths$res[[1]])$path <- 1



# following instructions:
# Paint nodes and edges on the path in red
# Use red for node labels
# Since this is a directed graph, add arrows to the edges
# First, set default values

# Name of this new style
style.name = "PathVisualization3"

# Define default values
def.node.color <- list(
  visualProperty = "NODE_FILL_COLOR",
  value = "#EEEEEE"
)

def.node.size <- list(
  visualProperty = "NODE_SIZE",
  value = 12
)

def.node.border.width <- list(
  visualProperty = "NODE_BORDER_WIDTH",
  value = 0
)

def.edge.width <- list(
  visualProperty = "EDGE_WIDTH",
  value = 2
)

def.edge.color <- list(
  visualProperty = "EDGE_STROKE_UNSELECTED_PAINT",
  value = "#aaaaaa"
)

def.edge.target.arrow.color = list(
  visualProperty="EDGE_TARGET_ARROW_UNSELECTED_PAINT",
  value = "#aaaaaa"
)

def.node.labelposition <- list(
  visualProperty = "NODE_LABEL_POSITION",
  value = "S,NW,c,6.00,0.00"  
)

def.edge.target.arrow <- list(
  visualProperty="EDGE_TARGET_ARROW_SHAPE",
  value="ARROW"
)

defaults <- list(def.node.color, def.node.size, def.node.labelposition, 
                 def.edge.color, def.node.border.width, def.edge.target.arrow, 
                 def.edge.width, def.edge.target.arrow.color)
```

# It looks complicated, but actually it's simple. 
# All you have to do is defining key-value pair for each visual variables you 
# want to change.  It's just like CSS.

##### Mappings
# __Visual Mapping__ is the most important concept in Cytoscape.  
# In Cytoscape, __data controls the view__  
# This means, once mapping is set, color, size, shape, or all other visual variables 
# are controlled by one of the column values in your data table
# Let's define a simple mapping: _if path value is 1, use RED for painting objects_


# Visual Mappings
mappings = list()

# Actual mapping.  This is a reusable component.
pair1 = list(
  key = "1",
  value = "red"
)

# This mapping object is also reusable!
discrete.mappings = list(pair1)

# Assign the map for node, edge, and label colors.
node.color = list(
  mappingType="discrete",
  mappingColumn="path",
  mappingColumnType="Integer",
  visualProperty="NODE_FILL_COLOR",
  map = discrete.mappings
)

node.label.color = list(
  mappingType="discrete",
  mappingColumn="path",
  mappingColumnType="Integer",
  visualProperty="NODE_LABEL_COLOR",
  map = discrete.mappings
)

edge.color = list(
  mappingType="discrete",
  mappingColumn="path",
  mappingColumnType="Integer",
  visualProperty="EDGE_STROKE_UNSELECTED_PAINT",
  map = discrete.mappings
)

edge.target.arrow.color = list(
  mappingType="discrete",
  mappingColumn="path",
  mappingColumnType="Integer",
  visualProperty="EDGE_TARGET_ARROW_UNSELECTED_PAINT",
  map = discrete.mappings
)

# One more instruction.  Use "name" column for label.
node.label = list(
  mappingType="passthrough",
  mappingColumn="name",
  mappingColumnType="String",
  visualProperty="NODE_LABEL"
)

mappings = list(node.color, node.label, node.label.color, edge.color, edge.target.arrow.color)

style <- list(title=style.name, defaults = defaults, mappings = mappings)
style.JSON <- toJSON(style)

style.url = paste(base.url, "styles", sep="/")
POST(url=style.url, body=style.JSON, encode = "json")


# Send everything back to Cytoscape and visualize it
send2cy(toCytoscape(g.tca), style.name, 'circular')

# Now you see the path!
![](http://cl.ly/WyVf/tca_path_final.png)
