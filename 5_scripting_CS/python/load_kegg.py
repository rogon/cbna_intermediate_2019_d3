from java.io import File
KEGG_DIR = "/ABS_PATH_TO/carbohydrate/"
pathways = ["eco00010.xml", "eco00020.xml", "eco00030.xml", "eco00040.xml", "eco00051.xml", "eco00052.xml", "eco00053.xml", "eco00500.xml", "eco00520.xml", "eco00562.xml", "eco00620.xml", "eco00630.xml", "eco00640.xml", "eco00650.xml", "eco00660.xml"]

loadNetworkTF = cyAppAdapter.get_LoadNetworkFileTaskFactory() 
taskManager = cyAppAdapter.getTaskManager()
allTasks = None
for pathway in pathways:
	kgmlpath = File(KEGG_DIR + pathway)
	print str(kgmlpath)
	itr = loadNetworkTF.createTaskIterator(kgmlpath) 
	if allTasks is None:
        allTasks = itr
    else:
		allTasks.append(itr) 
taskManager.execute(allTasks)
