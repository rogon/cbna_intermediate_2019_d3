# python2 -m pip install requests
# python2 get_ecoli.py 
import requests
ORGANISM = "eco"
pathways = requests.get('http://rest.kegg.jp/list/pathway/' + ORGANISM) 
for line in pathways.content.split('\n'):
	pathwayid = line.split('\t')[0].replace('path:', '')
	kgml = requests.get('http://rest.kegg.jp/get/' + pathwayid + '/kgml') 
	f = open(pathwayid + '.xml', 'w')
	f.write(kgml.content)
	f.close
