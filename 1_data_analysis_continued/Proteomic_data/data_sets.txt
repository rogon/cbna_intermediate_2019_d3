BP: Pairwise gene semantic similarities (Resnik score) in the biological process domain
CODA80: Predicted protein interactions from domain co-occurrence/fusion events
HIPPO: Protein interactions transferred from model organisms to human by orthology
PI: Protein interactions in human (from multiple databases)
TM: Protein interactions derived from the iHOP text mining method
