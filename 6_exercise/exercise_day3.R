#Exercise Day 3

#Open the Ash_net_source.txt
# -  This file contains a hit list with experimental measurements 
# -	load it into R dataframe

ash_net_source <- read.delim("/Volumes/Work/01. Teaching/CBNA Courses/Level 2-3 Graph Theory, Advanced scripting and automation/2019/Day 3 - cyREST/6_exercise/ash_net_source_students.txt", stringsAsFactors=FALSE)
 
# -	Download String interactions for the whole dataset without thresholding
 
string_db <- STRINGdb$new( version="10", species=10090, score_threshold=0, input_directory="" )
main_string_mapping = string_db$map(ash_net_source, "ensembl_gene", removeUnmappedRows = TRUE )
main_string_interactions <- string_db$get_interactions(main_string_mapping$STRING_id)
dataset_string <- main_string_interactions
dataset_string$from <- gsub("10090.", "", dataset_string$from, fixed=TRUE)
dataset_string$to <- gsub("10090.", "", dataset_string$to, fixed=TRUE)

# -	Using iGraph

# convert network to igraph object
network <- graph.data.frame(dataset_string, directed=F)
E(network)$combined_score <- E(network)$combined_score/1000

#	-	filter the network at 0, 0.4, and 0.7

network_0.4 <- delete.edges(network, which(E(network)$combined_score <0.4))
network_0.7 <- delete.edges(network, which(E(network)$combined_score <0.7))
network_0.9 <- delete.edges(network, which(E(network)$combined_score <0.9))

# -	check if the network is simple, simplify if needed
is.simple(network)
is.simple(network_0.4)
is.simple(network_0.7)

# remove disconnected nodes
network_0.4 = delete.vertices(network_0.4,which(degree(network_0.4)<1))
network_0.7 = delete.vertices(network_0.7,which(degree(network_0.7)<1))
network_0.9 = delete.vertices(network_0.9,which(degree(network_0.9)<1))
summary(network_0.4)
summary(network_0.7)
summary(network_0.9)


# Extract largest component
# either use clusters
clusters(network_0.9)

# or decompose graph
dg <- decompose.graph(network_0.9)
plot(dg[[1]])

#	-	evaluate eigenvector, closeness, radiality, diameter, beetwenness, and degree prior and after filtering
#	-	list top 10 nodes according to eigenvector, closeness, betweenness and degree 
#			- you can use just ordering like code below or assuming we're looking for the highest values across all, use a combination of all in a single score by summarizing

V(network_0.9)$degree <- degree(network_0.9)
V(network_0.9)$closeness <- closeness(network_0.9)
V(network_0.9)$betweenness <- betweenness(network_0.9)
V(network_0.9)$eigenvector <- eigen_centrality(network_0.9, directed=F, weights=E(network_0.9)$combined_score)$vector
V(network_0.9)$page_rank <- page.rank(network_0.9)$vector


# igraph object 
# string_graph 
my_funcs <- list('closeness'= closeness, 'betweenness' = betweenness, 'degree' = degree, 'eigenvector' = eigenvector)
lapply(my_funcs, function(x) x(network_0.9))
lapply(my_funcs, function(x) {
	l <- x(network_0.9)
	l <- l[order(l, decreasing = T)[1:10]]
	return(l)
})


#	-	duplicate network and on the copy delete nodes with mean log2 fold change
#		across all 120OFF hours if the value for
#			- upregulated is lower than 0.2 
#			- downregulated is higher than -0.2
		
my_means <- apply(ash_net_source[,grep(pattern="log2fc_120hOFF"), fixed = T, value = T, x = colnames(ash_net_source))], 1, mean, na.rm =T)


# -	Load 23 node hit list from query_list.txt
# -	download string interactions
#	- how connected is the network? How can you investigate connectivity? Look at cohesion
query_list <- read.delim("/Volumes/Work/01. Teaching/CBNA Courses/Level 2-3 Graph Theory, Advanced scripting and automation/2019/Day 3 - cyREST/6_exercise/query_list.txt", header=FALSE, stringsAsFactors=FALSE)
query_mapping = string_db$map(query_list, "V3", removeUnmappedRows = TRUE )
query_interactions <- string_db$get_interactions(query_mapping$STRING_id)
query_string <- query_interactions
query_string$from <- gsub("10090.", "", query_string$from, fixed=TRUE)
query_string$to <- gsub("10090.", "", query_string$to, fixed=TRUE)

query_network <- graph.data.frame(query_string, directed=F)

# -	Take the two networks you've created - query_list and Ash_net_source  - and
#	- merge them using igraph Union (use either the operator %u% or the function union, can you see a difference)
plot(query_network, vertex.size=10, vertex.label=NA)
plot(dg[[1]], vertex.size=10, vertex.label=NA)
plot(query_network %du% dg[[1]], vertex.size=10, vertex.label=NA)
plot(query_network %u% dg[[1]], vertex.size=10, vertex.label=NA)

net2 <- igraph::union(query_network, network_0.9)
plot(net2, vertex.label=NA, layout=layout_nicely)
summary(net2)
summary(network_0.9)
summary(query_network)

# -	annotate nodes with entrez id's
# Biomart
library("biomaRt")
ensembl = biomaRt::useMart("ensembl")
ensembl = useDataset("mmusculus_gene_ensembl", mart=ensembl)
datasets_biomart <- listDatasets(ensembl)
attributes_biomart <- listAttributes(ensembl)
# string mapper (e.g. main_string_mapping, or query_mapping) already contains gene to protein annotation

# use as filters protein id, located in the name attribute of your union network
union_network_results= getBM(attributes = c("mgi_symbol","entrezgene_id", "ensembl_peptide_id"),
               filters = c(filters = "ensembl_peptide_id"),
               values =  V(net2)$name,
               mart = ensembl)


# - run enrichment analysis against KEGG and Reactome
#	- Extract top pathway

# clusterProfiler
library(clusterProfiler)
library(pathview)
library(org.Mm.eg.db)
library(DOSE)

# extract entrez id's from the annotation frame 'results' created with BioMart earlier
list_Entrez <- as.list(union_network_results$entrezgene_id)
kk <- enrichKEGG(list_Entrez, organism="mouse", pvalueCutoff=0.05, pAdjustMethod="BH", qvalueCutoff=0.1)
enrichKegg_results <- as.data.frame(kk)
mmu03030 <- pathview(gene.data = as.character(list_Entrez), pathway.id = "mmu03030",species = "mouse")



# repeat for gene ontology
kg <- enrichGO(list_Entrez, OrgDb = "org.Hs.eg.db", ont="BP", pvalueCutoff=0.05, pAdjustMethod="BH", qvalueCutoff=0.1, minGSSize = 5)
head(summary(kg))
summary(kg)

library(enrichplot)

# cnetplot depicts the associations of genes with biological concepts 
# i.e. gene to term (GO/pathway)
cnetplot(kk)

# relationships between terms
emapplot(kk)

# The upsetplot is an alternative to cnetplot for visualizing the complex association 
# between genes and gene sets. It emphasizes the gene overlapping among different gene
# sets.
upsetplot(kk)
heatplot(kk)
dotplot(kk)
barplot(kk, showCategory = 15)
dotplot(kg)
barplot(kg, showCategory = 15)

write.table(kk, file="kegg_on_hits.txt", row.names = F, quote = FALSE, sep="\t")
write.table(kg, file="go_on_hits.txt", row.names = F, quote = FALSE, sep="\t")


result <- summary(kk)
write.table(result, file="KEGG_enrichment_2019.txt", row.names = F, quote = FALSE, sep="\t")

# export entrez ID's for the hit list (will be used for filtering in Cytoscape)
lapply(list_Entrez, write, "entrez_list_hits_2019.txt", append=TRUE, ncolumns=1000)













 - transfer the resulting merged and notch networks to Cytoscape
	- use cyREST
	- or graphml file export

 - perform community detection
	- you can do this by using the ClusterONE community detection in Cytoscape or
	- or by using community detection in R and transfering the results to Cytoscape
	- Look at the Advanced iGraph scripts - there you will find implemented community detection algorithms
	
 - visualize communities on your Cytoscape network 
	- either by using Style in Cytoscape
	- or by creating your own custom style in R and transferring to CS via the REST interface

