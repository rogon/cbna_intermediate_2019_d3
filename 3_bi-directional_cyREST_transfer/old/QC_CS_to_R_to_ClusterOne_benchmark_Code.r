# Matt Rogon, 19.11.2019, EMBL, Centre for Network Analysis
# v.0.2
# 
# Data extraction, conversion, and benchmarking script for CS to R connection
#
# This script requires the following:
# - Cytoscape 3.x or higher running with cyREST (integrated from 3.3 or later)
# - Loaded CS session file with clustering result "only_clusters_for_extraction.cys"
# - R with RJSONIO, igraph, httr, and all other packages loaded below
# 
# Functionality:
# - setup network connection via the REST interface
# - setup URL's for jSON data extraction and conversion -
# - extraction from CS for all networks into output files
# - preparation of the output cluster file for benchmarking with ClusterONE script
# - terminal-executable code for python benchmarking script (match_standalone_py) 
# 
# match_standalone_py is part of ClusterONE package: for details refer to the authors page
# http://www.paccanarolab.org/static_content/clusterone/additional_information.html


# pre-R quality control in CS
#19 was duplicated
#24 missing
#38 missing


# /Volumes/Work/07. Projects/Francis/For Inference/edges/Rattle 02/03. networks/05. Sent to everyone/Network_0.85_clustered.cys
setwd("/Volumes/Work/07. Projects/Francis/For Inference/edges/Rattle 02/03. networks/06. ClusterOne on RF_probability - Final no redundancy clusters (Francis)")

# Extract all networks from cytoscape into R using cyREST
library(RJSONIO)
library(igraph)
library(httr)

# Basic settings
port.number = 1234
base.url = paste("http://localhost:", toString(port.number), "/v1", sep="")
print(base.url)
version.url = paste(base.url, "version", sep="/")
cytoscape.version = GET(version.url)
cy.version = fromJSON(rawToChar(cytoscape.version$content))
print(cy.version)

url_networks = paste(base.url, "networks.names", sep="/")
names <- fromJSON(url_networks)
# names[[1]]$name

# json_data now contains names and suid of all networks:
json_data <- as.data.frame(do.call(rbind, names))

# format request URL for edges pull-down
#url_networks = paste(base.url, "networks.names", sep="/")

# count networks
n <- length(json_data$SUID)

# all I need is a node list for each cluster separately
#/v1/networks/networkId/nodes/
node_extraction_all = paste(base.url, "networks", json_data$SUID[1:n], "nodes","", sep="/")
outlist <- list(fromJSON(node_extraction[1]))
all.results=data.frame()

# for each network in my complete list of json_data suid's extract
# nodes into a separate file
for(k in 1:n)
{
     # iterating network number
     nodes <- fromJSON(node_extraction_all[k])
     num_nodes <- length(nodes)

     # iterating network number
     node_extraction = paste(base.url, "networks", json_data$SUID[k], "nodes", nodes[1:num_nodes],"", sep="/")

     # for the selected k network we have the proper URL in node_extraction, now from each of those URLs
     # we're extracting the json data stream - this is a list containing the needed data
     #outlist ends up being a list of lists
     for(i in 1:num_nodes)
     {
          outlist[i] <- list(fromJSON(node_extraction[i]))
     }

     # access with
     # outlist[[1:num_nodes]]$data$name
     # test the extraction
     #text <- as.data.frame(outlist[[1]]$data$name, "1")

     # define text2 data frame for storage
     text2 = data.frame(stringsAsFactors=FALSE)

     # now from each stored json list we're extracting the node Uniprot ID
     for(i in 1:num_nodes)
     {
              text <- as.data.frame(outlist[[i]]$data$name)
              # append to text2 data frame without overwriting
              text2 <- rbind(text2, text)
     }

    # rename main column
    names(text2)[names(text2) == 'outlist[[i]]$data$name'] <- 'nodes'

  # create new column with cluster name
    text2$cluster <- names[[k]]$name
  # create new column with cluster number
   text2$clusterNum <- k

  filename = paste("nodes_all_networks/", k, ".txt",sep="")
    all.results=rbind(all.results,text2)

     # iterating network number
     #write.table(text2, file="3.txt", row.names = F, sep="\t")
   #write.table(text2, file=filename, row.names = F, sep = "\t", quote=FALSE)
}

# as numeric to fill in numbers instead of a list which comes out of lapply/strsplit combo
all.results$FrancisID <- as.numeric(lapply(strsplit(as.character(all.results$cluster), "_"), "[", 1))

# output file
write.table(all.results, file="all_results.txt", row.names = F, sep = "\t", quote=FALSE)

# convert from LONG to WIDE by collapsing non-unique rows and merging the 1 column into many per cell
# unique rows are the result
# solution from http://stackoverflow.com/questions/17707389/collapsing-rows-in-a-data-frame-while-merging-another-column-value
wide_format <- ddply(all.results, .(cluster),
   summarise,
      nodes = paste(unique(code1),collapse = ' '),
      clusterNum = clusterNum[1],
       FrancisID = FrancisID[1])
write.table(wide_format, file="all_results_wide.txt", row.names = F, sep = "\t", quote=FALSE)

# export table for ClusterONE benchmarking with match_standalone.py
write.table(wide_format$nodes, file="Francis_clusters_for_match_standalone_py.txt", row.names = F, sep = " ", quote=FALSE)

# datafile added to root of reproducibility for ClusterONE, as results.txt
# Evaluate the quality of the clustering
# python scripts/match_standalone.py -n datasets/interactions.txt gold_standard/gold_standard_for_match_fin.txt results.txt > results_eval.txt

#  frac = 0.6897
#  cws = 0.9061
#  ppv = 0.7657
#  acc = 0.8330
#  mmr = 0.4595

#679 known proteins found in network 29 reference complexes, 66 predicted complexes
# now modify the code for shared_name

